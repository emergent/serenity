# Serenity: A meditative music

An ambient composition for SuperCollider, written between September and November 2023. 

These files are the sourcecode to my composition. I do my best to check if the code is working as intended before publication; however, I cannot give any guarantees that the information they contain is complete and accurate, nor can I guarantee that they will produce the intended results on your machine.

## License
Code licensed under the [Peer Production License](https://civicwise.org/peer-production-licence-_-human-readable-summary/).

## Recording
Recording available on [BandCamp](https://emergent.bandcamp.com/).

## Dedication
For my sweet and supportive wife Willow Wedemeyer, with love and gratitude <3
