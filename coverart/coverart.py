from turtle import *

hideturtle()
speed(0)
#pensize(2)
colormode(255)
bgcolor("#101010")

for i in range(50):
    pencolor(((i*3), (i*4), (i*5)))
    circle(200)
    penup()
    forward(2 + i)
    left(17)
    pendown()
    
ts = getscreen()
ts.getcanvas().postscript(file = "circles.eps")